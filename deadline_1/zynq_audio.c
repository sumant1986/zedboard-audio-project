/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "audio.h"
#include "oled.h"
#include "sleep.h"
#include <stdlib.h>
#include "complex.h"    /* Standard Library of Complex Numbers */
//void print(char *str);

#include <math.h>
#include "arm_neon.h"

void BitReversed_FFT_Algorithm(void);

Xint16 audio_data[128];


#define NPtDFT 128
#define LastStage NPtDFT>>1

double complex FFT_Calculated_Array[NPtDFT];

struct Magnitude_Array
{
	Xint32 FFT_Magnitude_Array[NPtDFT];
}Window_No[8];

u16 FFT_Averaging_Array[NPtDFT];

double complex Twiddle_Factor[64];
double complex FFT_Lower_Element = 0+I*0;
double complex FFT_Upper_Element = 0+I*0;
u8 Bit_Reversed_Array[NPtDFT];
#define Noise_Lower_Thr 4
#define Noise_Upper_Thr 60

#define timer_base 0xF8F00000
static volatile int *timer_counter_l=(volatile int *)(timer_base+0x200);
static volatile int *timer_counter_h=(volatile int *)(timer_base+0x204);
static volatile int *timer_ctrl=(volatile int *)(timer_base+0x208);
int main()
{
	int i;

	u8 twiddle_LUT=0;

	u8 Lower_Index =0;
	u8 Block_Stage_Cal = 0;
	u8 Stage_Cal = 0;

	u8 N =0;
	u8 j=0;
	u8 k=0;

	u8 Stage_DFT_new =0;
	u8 temp_power_new = 0;

	u8 Window_Size = 0;
	u8 audio_data_index = 0;
	u8 temp_y0=0;
	u8 temp_y1=0;
	u8 temp_y2=0;
	u8 temp_y3=0;
	u8 temp_y4=0;
	u8 temp_y5=0;
	u8 temp_y6=0;
	u8 reversed_value = 0;

	u8 *oled_equalizer_buf=(u8 *)malloc(128*sizeof(u8));
	Xil_Out32(OLED_BASE_ADDR,0xff);
	OLED_Init();			//oled init
	IicConfig(XPAR_XIICPS_0_DEVICE_ID);
	AudioPllConfig(); //enable core clock for ADAU1761
	AudioConfigure();

	xil_printf("ADAU1761 configured\n\r");


	/****** Preparation of LUT for the Twiddle factor ******/
	for (twiddle_LUT = 0;twiddle_LUT<LastStage;twiddle_LUT++)
	{
		Twiddle_Factor[twiddle_LUT]=cos((2*M_PI/NPtDFT)*twiddle_LUT)-(I*sin((2*M_PI/NPtDFT)*twiddle_LUT));
	}

	/****** Preparation for the Bit reversed value ************/
	for(audio_data_index = 0; audio_data_index<NPtDFT;audio_data_index++)
	{
		temp_y0 = (audio_data_index & 0x01)<<6;
		temp_y6 = (audio_data_index & 0x40)>>6;
		temp_y1 = (audio_data_index & 0x02)<<4;
		temp_y5 = (audio_data_index & 0x20)>>4;
		temp_y2 = (audio_data_index & 0x04)<<2;
		temp_y4 = (audio_data_index & 0x10)>>2;
		temp_y3 = (audio_data_index & 0x08);
		reversed_value = temp_y0 | temp_y1 | temp_y2 | temp_y3 | temp_y4 | temp_y5 | temp_y6;

		Bit_Reversed_Array[audio_data_index] = reversed_value;

	}

	while(1)
	{
		/**** Loop for the 128 samples for each window ***/
		while(Window_Size<8)
		{
			//init_timer(timer_ctrl, timer_counter_l, timer_counter_h);

            //start_timer(timer_ctrl);

			/*
			 * perform continuous read and writes from the codec that result in a loopback
			 * from Line in to Line out
			 */
			get_audio(audio_data);

			/*** Taking the Bit reversed value of audio input and doing the first stage FFT**/
			BitReversed_FFT_Algorithm();

			for(N=2;N<NPtDFT;N=N<<1)  /* stages*/
			{
				Stage_DFT_new = log2(N)+1;
				temp_power_new = (NPtDFT>>Stage_DFT_new);
				Stage_Cal = 2*N;

				for(j=0;j<(NPtDFT>>Stage_DFT_new);j++) /* Blocks*/
				{
					Block_Stage_Cal =  (j*Stage_Cal);
					for(k=0;k<N;k++) /* Butterfly */
					{
						Lower_Index = Block_Stage_Cal +k;  /** Lower Index calculation , Upper Index is always Lower_Index+N **/

						if(k!=0)  /** Whether the Twiddle factor is 1 or not **/
						{
							if(N==LastStage)  /** Last Stage of FFT **/
							{
								/** Calculation for all samples other than no 0 and 64 ***/
                                /*** FFT processing **/
								FFT_Calculated_Array[Lower_Index]= (FFT_Calculated_Array[Lower_Index] + ((Twiddle_Factor[temp_power_new * k])*FFT_Calculated_Array[Lower_Index+N]));

								/*** Magnitude Calculation a+ib = a^2+b^2 *********/
								Window_No[Window_Size].FFT_Magnitude_Array[Lower_Index] = FFT_Calculated_Array[Lower_Index] * conj(FFT_Calculated_Array[Lower_Index]);

                                /*** Averaging and Root Mean Square value calculation **/
								FFT_Averaging_Array[Lower_Index]=sqrt((Window_No[0].FFT_Magnitude_Array[Lower_Index] + Window_No[1].FFT_Magnitude_Array[Lower_Index] +Window_No[2].FFT_Magnitude_Array[Lower_Index] +
										Window_No[3].FFT_Magnitude_Array[Lower_Index] + Window_No[4].FFT_Magnitude_Array[Lower_Index] +Window_No[5].FFT_Magnitude_Array[Lower_Index] +Window_No[6].FFT_Magnitude_Array[Lower_Index] +
										Window_No[7].FFT_Magnitude_Array[Lower_Index])>>3);

								/************ Noise Cancellation part **********/

								if(FFT_Averaging_Array[Lower_Index]>Noise_Upper_Thr)
								{
									FFT_Averaging_Array[Lower_Index] = Noise_Upper_Thr;
								}
								else
								{
									FFT_Averaging_Array[Lower_Index] -= Noise_Lower_Thr;
								}

								FFT_Averaging_Array[NPtDFT-Lower_Index]=FFT_Averaging_Array[Lower_Index];

							}
							else
							{
								/*** FFT processing **/
								FFT_Lower_Element = FFT_Calculated_Array[Lower_Index];
								FFT_Upper_Element = FFT_Calculated_Array[Lower_Index+N];

								FFT_Calculated_Array[Lower_Index]= (FFT_Lower_Element + ((Twiddle_Factor[temp_power_new * k])*FFT_Upper_Element));
								FFT_Calculated_Array[Lower_Index+N]= (FFT_Lower_Element - ((Twiddle_Factor[temp_power_new * k])*FFT_Upper_Element));


							}


						}
						else
						{
							/*** FFT processing **/
							FFT_Lower_Element= FFT_Calculated_Array[Lower_Index];
							FFT_Upper_Element = FFT_Calculated_Array[Lower_Index+N];
							FFT_Calculated_Array[Lower_Index]= (FFT_Lower_Element + FFT_Upper_Element);
							FFT_Calculated_Array[Lower_Index+N]= (FFT_Lower_Element - FFT_Upper_Element);

							if(N==LastStage)
							{
								/** Calculation for sample no 0 and 64 ***/

								/*** Magnitude Calculation a+ib = a^2+b^2 *********/
								Window_No[Window_Size].FFT_Magnitude_Array[0] = FFT_Calculated_Array[0] * conj(FFT_Calculated_Array[0]);
								Window_No[Window_Size].FFT_Magnitude_Array[N] = FFT_Calculated_Array[N] * conj(FFT_Calculated_Array[N]);

								/*** Averaging and Root Mean Square value calculation **/
								FFT_Averaging_Array[0]=sqrt((Window_No[0].FFT_Magnitude_Array[0] + Window_No[1].FFT_Magnitude_Array[0] +Window_No[2].FFT_Magnitude_Array[0] +
										Window_No[3].FFT_Magnitude_Array[0] + Window_No[4].FFT_Magnitude_Array[0] +Window_No[5].FFT_Magnitude_Array[0] +Window_No[6].FFT_Magnitude_Array[0] +
										Window_No[7].FFT_Magnitude_Array[0])>>3);

								/************ Noise Cancellation part for sample no 0**********/
								if(FFT_Averaging_Array[0]>Noise_Upper_Thr)
								{
									FFT_Averaging_Array[0] = Noise_Upper_Thr;
								}
								else
								{
									FFT_Averaging_Array[0] -= Noise_Lower_Thr;
								}

								/*** Averaging and Root Mean Square value calculation **/
								FFT_Averaging_Array[N]=sqrt((Window_No[0].FFT_Magnitude_Array[N] + Window_No[1].FFT_Magnitude_Array[N] +Window_No[2].FFT_Magnitude_Array[N] +
										Window_No[3].FFT_Magnitude_Array[N] + Window_No[4].FFT_Magnitude_Array[N] +Window_No[5].FFT_Magnitude_Array[N] +Window_No[6].FFT_Magnitude_Array[N] +
										Window_No[7].FFT_Magnitude_Array[N])>>3);

								/************ Noise Cancellation part for sample no 64**********/
								if(FFT_Averaging_Array[N]>Noise_Upper_Thr)
								{
									FFT_Averaging_Array[N] = Noise_Upper_Thr;
								}
								else
								{
									FFT_Averaging_Array[N] -= Noise_Lower_Thr;
								}

							}

						}

					}
				}
			}

			//stop_timer(timer_ctrl);
			//xil_printf("Communication time %d us\n\r", (*timer_counter_l)/333);

			for(i=0;i<128;i++)
			{
                /** display on Oled **/
				oled_equalizer_buf[i]=FFT_Averaging_Array[i];
			}
			OLED_Clear();
			OLED_Equalizer_128(oled_equalizer_buf);

			if(7U == Window_Size) /** If 8 windows are done start with the first window again **/
			{
				Window_Size = 0;
			}
			else
			{
				Window_Size++;
			}
		}
	}
	return 0;
}

void BitReversed_FFT_Algorithm(void)
{
	Xint16 FFT_Lower_Element;
	Xint16 FFT_Upper_Element;

	/************** Bit Reversal and first stage FFT algorithm**********/
	u8 audio_data_index;

	u8 reversed_value;

	for(audio_data_index = 0; audio_data_index<NPtDFT;audio_data_index++)
	{

		reversed_value = Bit_Reversed_Array[audio_data_index];

		FFT_Lower_Element = audio_data[reversed_value];

		audio_data_index++;

		reversed_value = Bit_Reversed_Array[audio_data_index];


		FFT_Upper_Element = audio_data[reversed_value];

		FFT_Calculated_Array[audio_data_index-1]= FFT_Lower_Element+ FFT_Upper_Element;
		FFT_Calculated_Array[audio_data_index] = FFT_Lower_Element- FFT_Upper_Element;

	}

}
