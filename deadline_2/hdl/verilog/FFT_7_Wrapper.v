`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:17:03 10/09/2014 
// Design Name: 
// Module Name:    FFT_7_Wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FFT_7_Wrapper(
input clk,
input start,
input [15 : 0] xn_re,
input [15 : 0] xn_im,
input fwd_inv,
input fwd_inv_we,
input [13 : 0] scale_sch,
input scale_sch_we,
output rfd,
output [6 : 0] xn_index,
output busy,
output edone,
output done,
output dv,
output [6 : 0] xk_index,
output [15 : 0] xk_re,
output [15 : 0] xk_im
    );

wire rfd1;	 
wire [6 : 0] xn_index1;
wire busy1;
wire edone1;
wire done1;
wire dv1;
wire [6 : 0] xk_index1;
wire [15:0] xk_re1;
wire [15:0] xk_im1;
	 
FFT_7 your_instance_name (
  .clk(clk), // input clk
  .start(start), // input start
  .xn_re(xn_re), // input [15 : 0] xn_re
  .xn_im(xn_im), // input [15 : 0] xn_im
  .fwd_inv(fwd_inv), // input fwd_inv
  .fwd_inv_we(fwd_inv_we), // input fwd_inv_we
  .scale_sch(scale_sch), // input [13 : 0] scale_sch
  .scale_sch_we(scale_sch_we), // input scale_sch_we
  .rfd(rfd1), // output rfd
  .xn_index(xn_index1), // output [6 : 0] xn_index
  .busy(busy1), // output busy
  .edone(edone1), // output edone
  .done(done1), // output done
  .dv(dv1), // output dv
  .xk_index(xk_index1), // output [6 : 0] xk_index
  .xk_re(xk_re1), // output [15 : 0] xk_re
  .xk_im(xk_im1) // output [15 : 0] xk_im
);

assign rfd = rfd1;
assign xn_index[6 : 0] = xn_index1[6 : 0] ;
assign busy = busy1;
assign edone = edone1;
assign done = done1;
assign dv = dv1;
assign xk_index[6 : 0] = xk_index1[6 : 0];
assign xk_re[15 : 0] = xk_re1[15 : 0];
assign xk_im[15 : 0] = xk_im1[15 : 0];



endmodule
