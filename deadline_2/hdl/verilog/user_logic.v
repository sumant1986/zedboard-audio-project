//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Fri Oct 10 19:54:59 2014 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  // --USER ports added here 
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  IP2Bus_AddrAck,                 // IP to Bus address acknowledgement
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error,                   // IP to Bus error response
  Type_of_xfer                    // Transfer Type
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_SLV_AWIDTH                   = 32;
parameter C_SLV_DWIDTH                   = 32;
parameter C_NUM_MEM                      = 1;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
// --USER ports added here 
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [C_SLV_AWIDTH-1 : 0]           Bus2IP_Addr;
input      [C_NUM_MEM-1 : 0]              Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
output                                    IP2Bus_AddrAck;
output     [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data;
output                                    IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
output                                    Type_of_xfer;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

  // --USER nets declarations added here, as needed for user logic

parameter NUM_BYTE_LANES = (C_SLV_DWIDTH+7)/8;
reg [C_SLV_DWIDTH-1 : 0] mem_data_out [0 : C_NUM_MEM-1];
wire [7:0] mem_address;
wire mem_select;
wire mem_read_enable;
reg  [C_SLV_DWIDTH-1 : 0] mem_ip2bus_data;
reg mem_read_ack_dly1;
reg mem_read_ack_dly2; 
wire mem_read_ack; 
wire mem_write_ack; 
reg [7 : 0] ram [C_NUM_MEM-1 : 0][NUM_BYTE_LANES-1 : 0][0 :255];
reg [NUM_BYTE_LANES-1 : 0] write_enable [C_NUM_MEM-1 : 0];
reg [7 : 0] data_in [C_NUM_MEM-1 : 0][NUM_BYTE_LANES-1 : 0];
reg [7 : 0] data_out [C_NUM_MEM-1 : 0][NUM_BYTE_LANES-1 : 0];
reg [7 : 0] read_address;

integer i;
integer byte_index;
  // --USER logic implementation added here

  // instantiations of FFT wrapper module starts here
  	// Inputs
	//reg clk;
	wire [C_SLV_DWIDTH-1 : 0] new_xn;
	
	reg signed [15:0] realip[0:127];
	reg signed [15:0] imagip[0:127];
	

	
	reg start;
	reg signed [15:0] xn_re;
	reg signed [15:0] xn_im;
	reg fwd_inv;
	reg fwd_inv_we;
    reg [13 : 0] scale_sch;
    reg scale_sch_we;


	integer count;

	//integer sampleoutput;

	integer opdone;
	// Outputs
	wire rfd;
	wire [6:0] xn_index;
	wire busy;
	wire edone;
	wire done;
	wire dv;
	wire [6:0] xk_index;
	wire [15:0] xk_re;
	wire [15:0] xk_im;
	
  	// Instantiate the Unit Under Test (UUT)
	FFT_7_Wrapper uut (
		.clk(Bus2IP_Clk), 
		.start(start), 
		.xn_re(xn_re), 
		.xn_im(xn_im), 
		.fwd_inv(fwd_inv), 
		.fwd_inv_we(fwd_inv_we),
		.scale_sch(scale_sch),
		.scale_sch_we(scale_sch_we),		
		.rfd(rfd), 
		.xn_index(xn_index), 
		.busy(busy), 
		.edone(edone), 
		.done(done), 
		.dv(dv), 
		.xk_index(xk_index), 
		.xk_re(xk_re), 
		.xk_im(xk_im)
	);
	
	  wire [31:0] realMagchk;
      wire [31:0] avgopchk;
	  
  
	 Adder ad1(.a_re(xk_re),
              .b_im(xk_im),
			  .datavalid(dv),
			  .magindex(xk_index),
			  .magclk(Bus2IP_Clk),
              .c_out(realMagchk));
	
     
      Average av1(.avip(realMagchk),
	              .avdatavalid(dv),
    			  .avindex(xk_index),
                  .avclk(Bus2IP_Clk),
                  .avop(avgopchk));	 
  
	initial begin
		// Initialize Inputs
		//clk = 0;
		start = 0;
		xn_re[15:0] = 0;
		xn_im[15:0] = 0;
		fwd_inv = 0;
		fwd_inv_we = 0;
		scale_sch =0;
		scale_sch_we = 0;
		
	    count =0;
		
		//sampleoutput = 0;
		opdone =0;
		// Wait 100 ns for global reset to finish
		//#100;
        	
		// Add stimulus here
		//clk = 1;
		start = 0;
		xn_re[15:0] = 0;
		xn_im[15:0] = 0;
		fwd_inv = 1;
		fwd_inv_we = 1;
		scale_sch = 14'b00000000000000;
		scale_sch_we = 1;

	end
	

 	
	

// ------------------------------------------------------
// Example code to read/write user memory space
assign mem_select = Bus2IP_CS;
assign mem_read_enable = Bus2IP_RdCE[0];
assign mem_read_ack = (mem_read_ack_dly1 && (!mem_read_ack_dly2));
assign mem_write_ack = Bus2IP_WrCE[0];
assign mem_address = Bus2IP_Addr[9:2];
assign new_xn = Bus2IP_Data;


always @( posedge Bus2IP_Clk)
begin
    if(Bus2IP_Resetn == 0) 
    begin
      mem_read_ack_dly1 <= 0;
      mem_read_ack_dly2 <= 0;
    end
    else
    begin
      mem_read_ack_dly1 <= mem_read_enable;
      mem_read_ack_dly2 <= mem_read_ack_dly1;
    end
 end


always @(*) begin
  for (i=0;i<=C_NUM_MEM-1;i=i+1) begin
    for (byte_index=0;byte_index<=NUM_BYTE_LANES-1;byte_index=byte_index+1) begin
      write_enable[i][byte_index] <= Bus2IP_WrCE[i] && Bus2IP_BE[byte_index];
      data_in[i][byte_index] <= Bus2IP_Data[(byte_index*8) +: 8];
    end
  end
end


always @(posedge Bus2IP_Clk) 
begin
  for (i=0;i<=C_NUM_MEM-1;i=i+1)
  begin
    for (byte_index=0;byte_index<=NUM_BYTE_LANES-1;byte_index=byte_index+1)
    begin
      if(write_enable[i][byte_index] == 1)
      begin
        ram[i][byte_index][mem_address] <= data_in[i][byte_index];
		
		/*** samples input stored in an input array **/
		  realip[count/4] = new_xn[15:0];
          imagip[count/4] = new_xn[C_SLV_DWIDTH-1 : 16];
          count = count +1;
          if(count == 512)
          begin
	        count=0; 
	 
          	start =1;
			opdone =0;
	 
           end 
  
  
		
      end

	  if(dv ==1)
	  begin
	      opdone =1;

				
				ram[i][0][0 + xk_index] <= avgopchk[7:0];
		        ram[i][1][0 + xk_index] <= avgopchk[15:8];
		        ram[i][2][0 + xk_index] <= avgopchk[23:16];
		        ram[i][3][0 + xk_index] <= avgopchk[31:24];
				
				
				ram[i][0][128] <= 8'h55;
				ram[i][1][128] <= 8'h55;
				ram[i][2][128] <= 8'h55;
				ram[i][3][128] <= 8'h55;
				

		  
		  
	  end
      read_address <= mem_address;
	
    end
  end


end //always @(posedge Bus2IP_Clk)


always @(*)
begin
  for (i=0;i<=C_NUM_MEM-1;i=i+1)
  begin
    for (byte_index=0;byte_index<=NUM_BYTE_LANES-1;byte_index=byte_index+1)
    begin

      data_out[i][byte_index] <= ram[i][byte_index][read_address];

    end
  end

end //always @(*) begin

always @(*)
begin
  for (i=0;i<=C_NUM_MEM-1;i=i+1)
  begin
    for (byte_index=0;byte_index<=NUM_BYTE_LANES-1;byte_index=byte_index+1)
    begin
      mem_data_out[i][(byte_index*8) +: 8] <= data_out[i][byte_index];
    end
  end
end //always @(*) begin

always @(*)
begin
  
  case (mem_select) 
    1 : mem_ip2bus_data <= mem_data_out[0];

    default : mem_ip2bus_data <= 0;
  endcase
end


  //assign IP2Bus_Data  = (mem_read_ack == 1'b1) ? mem_ip2bus_data : 0;
  assign IP2Bus_Data  = ((mem_read_ack == 1'b1) && (opdone == 1)) ? mem_ip2bus_data : 0;
  assign IP2Bus_AddrAck = (mem_write_ack || (mem_read_enable && mem_read_ack));
  assign IP2Bus_WrAck = mem_write_ack;
  assign IP2Bus_RdAck = mem_read_ack;
  assign IP2Bus_Error = 0;

  // ------------------------------------------------------------
  // Example code to drive IP to Bus signals
  // ------------------------------------------------------------
	
/**** FFT input block***************/
	
	always@(*)
	begin
	if(rfd==1)
	begin
		 
		 xn_re = realip[xn_index];
		 xn_im = imagip[xn_index];
	
	end
	end


  
  
endmodule

/**** Magnitude Module***************/
module Adder(a_re, b_im, datavalid, magindex, magclk, c_out);



input signed [15:0] a_re;

input signed [15:0] b_im;
input datavalid;
input [6:0] magindex;
input magclk;


output [31:0] c_out;

wire dataselect;
wire [6:0] indexselect;
wire clkselect;


reg [31:0] MagOut [0:127];


integer magflag ;

assign dataselect  = datavalid;
assign indexselect  = magindex;
assign clkselect  = magclk;


initial
begin
magflag =0;


end

always@(posedge clkselect)
begin

if(dataselect ==1)
begin
	    MagOut[indexselect] = (a_re * a_re) + (b_im * b_im);
	   magflag = 1;
end
end

assign c_out = ((magflag == 1) && (dataselect ==1)) ? MagOut[indexselect] : 0;

endmodule

/**** Average Module***************/
module Average(avip,avdatavalid,avindex,avclk,avop);


input [31:0] avip;
input avdatavalid;
input [6:0] avindex;
input avclk;


output [15:0] avop;

wire avdataselect;
wire [6:0] avindexselect;
wire avclkselect;



reg [31:0] window0[0:127];
reg [31:0] window1[0:127];
reg [31:0] window2[0:127];
reg [31:0] window3[0:127];
reg [31:0] window4[0:127];
reg [31:0] window5[0:127];
reg [31:0] window6[0:127];
reg [31:0] window7[0:127];
reg [31:0] windowsum[0:127];
reg [31:0] windowavg[0:127];

reg [31:0] noisethr0[0:127];
reg [31:0] noisethr1[0:127];
reg [31:0] noisethr2[0:127];
reg [31:0] noisethr3[0:127];
reg [31:0] noisethr4[0:127];
reg [31:0] noisethr5[0:127];
reg [31:0] noisethr6[0:127];
reg [31:0] noisethr7[0:127];

wire [31:0] avinterm;
reg [31:0] avinput;

reg [31:0] avout[0:127];
reg [31:0] avsum[0:127];

integer windowno;
integer windowsumm
integer firsttime;
integer loopcnt;
integer avcnt;
integer cnt;
integer avflag;
integer newcnt;
integer noisestartflag;

assign avdataselect  = avdatavalid;
assign avindexselect  = avindex;
assign avclkselect  = avclk;
assign avinterm = avip;


initial
begin
windowno =0;
loopcnt = 0;
cnt =0;
newcnt =0;
windowsumm =1000;
firsttime = 1;
noisestartflag =0;
for(avcnt =0; avcnt<128;avcnt = avcnt+1)
begin
window0[avcnt] = 0;
window1[avcnt] = 0;
window2[avcnt] = 0;
window3[avcnt] = 0;
window4[avcnt] = 0;
window5[avcnt] = 0;
window6[avcnt] = 0;
window7[avcnt] = 0;
windowsum[avcnt] = 0;
windowavg[avcnt] = 0;
avout[avcnt] = 0;
avsum[avcnt] = 0;
noisethr0[avcnt] = 0;
noisethr1[avcnt] = 0;
noisethr2[avcnt] = 0;
noisethr3[avcnt] = 0;
noisethr4[avcnt] = 0;
noisethr5[avcnt] = 0;
noisethr6[avcnt] = 0;
noisethr7[avcnt] = 0;
avflag = 0;
end

end

always@(posedge avclkselect)
begin
if(avdataselect ==1)
begin
    avinput = avinterm;

    case(windowno)
	0:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr0[avindexselect] = avinput; 	 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window0[avindexselect] = avinput - windowavg[avindexselect];
			 if(window0[avindexselect] > 40)
			 begin
			     window0[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window0[avindexselect]  = 0;
	     end
	 end
	end 
	1:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr1[avindexselect] = avinput; 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window1[avindexselect] = avinput - windowavg[avindexselect];
			 if(window1[avindexselect] > 40)
			 begin
			     window1[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window1[avindexselect]  = 0;
	     end
	 end
	 end
	2:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr2[avindexselect] = avinput; 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window2[avindexselect] = avinput - windowavg[avindexselect];
			 if(window2[avindexselect] > 40)
			 begin
			     window2[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window2[avindexselect]  = 0;
	     end
	 end
	 end
	3:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr3[avindexselect] = avinput; 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window3[avindexselect] = avinput - windowavg[avindexselect];
			 if(window3[avindexselect] > 40)
			 begin
			     window3[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window3[avindexselect]  = 0;
	     end
	 end
	 end
	4:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr4[avindexselect] = avinput; 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window4[avindexselect] = avinput - windowavg[avindexselect];
			 if(window4[avindexselect] > 40)
			 begin
			     window4[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window4[avindexselect]  = 0;
	     end
	 end
	 end
	5:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr5[avindexselect] = avinput; 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window5[avindexselect] = avinput - windowavg[avindexselect];
			 if(window5[avindexselect] > 40)
			 begin
			     window5[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window5[avindexselect]  = 0;
	     end
	 end
	 end
	6:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr6[avindexselect] = avinput; 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window6[avindexselect] = avinput - windowavg[avindexselect];
			 if(window6[avindexselect] > 40)
			 begin
			     window6[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window6[avindexselect]  = 0;
	     end
	 end
	 end
	7:
	begin
	 if(firsttime == 1)
	 begin
	     noisethr7[avindexselect] = avinput; 
	 end
	 else
	 begin
	     if(avinput > windowavg[avindexselect])
	     begin
	         window7[avindexselect] = avinput - windowavg[avindexselect];
			 if(window7[avindexselect] > 40)
			 begin
			     window7[avindexselect] = 40;
			 end
	     end
	     else
	     begin
	         window7[avindexselect]  = 0;
	     end
	 end
	 end
	default:
	begin
	end
	
	endcase
	
	
	
	if(firsttime  == 0)
	begin
	    avsum[avindexselect] = window0[avindexselect] + window1[avindexselect] + window2[avindexselect] + window3[avindexselect] +
	                               window4[avindexselect] + window5[avindexselect] + window6[avindexselect] + window7[avindexselect];
		avout[avindexselect] =  (avsum[avindexselect]/8);	
        loopcnt = 0;		
	end
	
	
	if(windowno == 7)
	begin
	    /*** Adaptive noise cancellation **/
	    if((firsttime == 1) && (loopcnt == 10))
		begin
	       windowsum[avindexselect]  = noisethr0[avindexselect] + noisethr1[avindexselect] + noisethr2[avindexselect] + noisethr3[avindexselect] +
	                               noisethr4[avindexselect] + noisethr5[avindexselect] + noisethr6[avindexselect] + noisethr7[avindexselect];
								   
			
			windowavg[avindexselect] = windowsumm;
            			
			

			
		    
			if(avindexselect == 127)
		    begin
			    cnt = cnt +1;
			    loopcnt = 0;
	            firsttime = 0;
				avflag =1;

				
		    end		
		
		end	
        if(avindexselect == 127)
        begin
		   if(loopcnt == 10)
		   begin
		   end
		   else
		   begin
		   loopcnt = loopcnt +1;
		   end
	       windowno = 0;
        end		
	    
	end
	else
	begin
	    if(avindexselect == 127)
		begin
	    windowno = windowno + 1;
		end
	end

end

end


assign avop = ((avdataselect ==1) && (avflag ==1)) ? avout[avindexselect] : 0;


endmodule